package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.DiscountCard;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
@RequestMapping("/customers")
public class CustomerController implements CustomersApi{

    @PostMapping("/{customerId}/discountcards")
    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(@ApiParam(value = "ID of customer",required = true) @PathVariable("customerId") Long customerId, @ApiParam(value = "DiscountCard object that needs to be added to the customer",required = true) @RequestBody @Valid DiscountCard body) {
        this.getRequest().ifPresent((request) -> {
            Iterator var1 = MediaType.parseMediaTypes(request.getHeader("Accept")).iterator();

            while(var1.hasNext()) {
                MediaType mediaType = (MediaType)var1.next();
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"validFor\" : \"30d\", \"customerId\" : 6, \"id\" : 0, \"validFrom\" : \"1992-01-01\", \"type\" : 1 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }

        });
        return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
    }

    @GetMapping("/{customerId}/discountcards")
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(@ApiParam(value = "ID of customer to search for discount cards",required = true) @PathVariable("customerId") Long customerId) {
        List<DiscountCard> customerDiscountCards = new ArrayList<DiscountCard>();

        DiscountCard testCard = new DiscountCard();
        testCard.setCustomerId(new Long(55443327));
        testCard.setType(1);

        customerDiscountCards.add(testCard);

        return ResponseEntity.ok().body(customerDiscountCards);
    }

}
